INTRODUCTION
------------

# User Active Indicator

## Description
This module shows an active mark and a last access timestamp next to the username.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/user_active_indicator

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/user_active_indicator


REQUIREMENTS
------------

This module requires the following modules:

 * User


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   
1. **Install** this module


AVAILABLE OPTIONS
------------

1. Theme username field
2. Theme user page title
3. Show an active <mark>
4. Show a last access timestamp
5. Customize text options
6. Define active user duration


CONFIGURATION
------------

1. Configure at `/admin/config/tinsel-suite/user-active-indicator`


MAINTAINERS
-----------

Current maintainers:
 * Preston Schmidt - https://www.drupal.org/user/3594865
